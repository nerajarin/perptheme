<%@ page contentType="text/css;charset=UTF-8" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.idempiere.org/dsp/web/util" prefix="u" %>

html,body {
	margin: 0;
	padding: 0;
	height: 100%;
	width: 100%;
	background-color: #D4E3F4;
	color: #333;
	font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;
	overflow: hidden;
}

.z-html p{
	margin:0px;
}

[class*="z-"]:not([class*="z-icon-"]):not([class*="z-group-icon-"]) {
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;
}
@media screen and (min-device-width: 2500px) {
	[class*="z-"]:not([class*="z-icon-"]):not([class*="z-group-icon-"]) {
		font-size: 15px;
	}
}
@media screen and (max-device-width: 2499px) {
	[class*="z-"]:not([class*="z-icon-"]):not([class*="z-group-icon-"]) {
		font-size: 14px;
	}
}
@media screen and (max-device-width: 1899px) {
	[class*="z-"]:not([class*="z-icon-"]):not([class*="z-group-icon-"]) {
		font-size: 13px;
	}
}
@media screen and (max-device-width: 1399px) {
	[class*="z-"]:not([class*="z-icon-"]):not([class*="z-group-icon-"]) {
		font-size: 12px;
	}
}
.z-datebox-button [class*="z-icon-"], .z-bandbox-button [class*="z-icon-"], .z-combobox-button [class*="z-icon-"] {
	font-size: 18px;
}

<%-- Mobile/Tablet --%>
.tablet-scrolling {
	-webkit-overflow-scrolling: touch;
}
<%-- default font size for mobile --%>
.mobile [class*="z-"]:not([class*="z-icon-"]):not([class*="z-group-icon-"]) {
    font-size: 16px;
}
<%-- the not=xyz is needed to get this selected over standard zk rule --%>
.mobile [class*="z-icon-"]:not([class*="xyz"]), .mobile [class*="z-group-icon-"] {
    font-size: 16px;
}

<%-- vbox fix for firefox and ie --%>
table.z-vbox > tbody > tr > td > table {
	width: 100%;	
}

<%-- workflow activity --%>
.workflow-activity-form {
}
.workflow-panel-table {
	border: 0px;
}

<%-- payment form --%>
.payment-form-content {
}

<c:include page="fragment/login.css.dsp" />

<c:include page="fragment/desktop.css.dsp" />

<c:include page="fragment/application-menu.css.dsp" />

<c:include page="fragment/gadget.css.dsp" />

<c:include page="fragment/toolbar.css.dsp" />

<c:include page="fragment/button.css.dsp" />

<c:include page="fragment/adwindow.css.dsp" />
			
<c:include page="fragment/grid.css.dsp" />

<c:include page="fragment/input-element.css.dsp" />

<c:include page="fragment/tree.css.dsp" /> 

<c:include page="fragment/field-editor.css.dsp" />

<c:include page="fragment/group.css.dsp" />

<c:include page="fragment/tab.css.dsp" />

<c:include page="fragment/menu-tree.css.dsp" />

<c:include page="fragment/info-window.css.dsp" />

<c:include page="fragment/window.css.dsp" />

<c:include page="fragment/form.css.dsp" />

<c:include page="fragment/toolbar-popup.css.dsp" />	

<c:include page="fragment/setup-wizard.css.dsp" />

<c:include page="fragment/about.css.dsp" />

<c:include page="fragment/tab-editor.css.dsp" />

<c:include page="fragment/find-window.css.dsp" />

<c:include page="fragment/help-window.css.dsp" />

<c:include page="fragment/borderlayout.css.dsp" />

<c:include page="fragment/parameter-process.css.dsp" />

<c:include page="fragment/window-size.css.dsp" />

<c:include page="fragment/font-icons.css.dsp" />

<c:if test="${u:isThemeHasCustomCSSFragment()}">
    <c:include page="fragment/custom.css.dsp" />
</c:if>

.z-north, .z-south, .z-west, .z-center, .z-east {
    border: none;
    background: #FFFFFF;
}
.z-north-body, .z-south-body, .z-west-body, .z-center-body, .z-east-body {
    padding: 0px !important;
}

.z-east-splitter, .z-west-splitter, .z-north-splitter, .z-south-splitter {
    width: 4px;
    /* background-color: #F9FCFF; */
    
}
z-popup-content{
	background-color: #062639 !important;
}
.z-panel-header {
  	color: #333333;
    font-weight: normal;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 1rem;
}

.desktop-header {
	background: linear-gradient(to right,#062639,#086096,#062639);
    border-bottom: 0px !important;
    box-shadow: 0 1px 5px 1px rgba(0,0,0,.45);
}
.desktop-header.mobile {
	height: 46px; /* 36px to 40px */
	padding: 4px 4px;
}

.desktop-left-column + .z-west-splitter, .desktop-left-column.z-west {
    border-top: none;
    border-right: none !important;
}
.desktop-left-column {
    width: 200px;
    border: none !important;
    border-right: none;
    background-color: #e9ebee;
   // background-image: linear-gradient(#086096, transparent);
    
}

.desktop-right-column + .z-east-splitter, .desktop-right-column.z-east {
    border-top: none;
    border-left: none !important;
}
.desktop-right-column {
    border: none !important;
    border-left: none;
    background-image: linear-gradient(#FFFFFF, transparent);
    padding-left: 4px;
}
.desktop-center{
	padding-top: 0px;
}
.desktop-tabbox.z-tabbox{
	border-top: 1px solid #C5C5C5;
	border-radius: 0px;
}
.dashboard-widget.z-panel{
	border-radius:4px;
}
