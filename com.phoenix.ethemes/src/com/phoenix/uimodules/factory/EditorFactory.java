package com.phoenix.uimodules.factory;

import org.adempiere.webui.editor.IEditorConfiguration;
import org.adempiere.webui.editor.WEditor;
import org.adempiere.webui.factory.IEditorFactory;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;

import com.phoenix.uimodules.editor.BSDatepicker;
import com.phoenix.uimodules.editor.NepaliDatepicker;
import com.phoenix.uimodules.editor.NepaliInput;

public class EditorFactory implements IEditorFactory{

	@Override
	public WEditor getEditor(GridTab gridTab, GridField gridField, boolean tableEditor) {
		System.out.println("gridfield old factory"+gridField);
		if(gridField == null) {
			return null;
		}
		
		WEditor editor = null;
		
		int displayType = gridField.getDisplayType();
		
		if(displayType == DisplayTypeFactory.NepaliDatePicker) {
			editor = new NepaliDatepicker(gridField);
		} 
		
		if (displayType == DisplayTypeFactory.NepaliInput) {
			editor = new NepaliInput(gridField);
		}
		
		if(displayType == DisplayTypeFactory.BSDatePicker) {
			editor = new BSDatepicker(gridField);
		} 
		
		

		if(editor != null) {
			editor.setTableEditor(tableEditor);
		}
		return editor;
	}

	@Override
	public WEditor getEditor(GridTab gridTab, GridField gridField, boolean tableEditor,
			IEditorConfiguration editorConfiguration) {
		if(gridField == null) {
			return null;
		}
		
		WEditor editor = null;
		
		int displayType = gridField.getDisplayType();
		
		if(displayType == DisplayTypeFactory.NepaliDatePicker) {
			editor = new NepaliDatepicker(gridField);
		} 
		
		if (displayType == DisplayTypeFactory.NepaliInput) {
			editor = new NepaliInput(gridField);
		}

		if(displayType == DisplayTypeFactory.BSDatePicker) {
			editor = new BSDatepicker(gridField);
		} 
		
		if(editor != null) {
			editor.setTableEditor(tableEditor);
		}
		return editor;
	}

}