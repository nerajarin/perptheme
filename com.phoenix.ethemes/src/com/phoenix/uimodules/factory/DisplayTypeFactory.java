package com.phoenix.uimodules.factory;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import org.adempiere.base.IDisplayTypeFactory;
import org.compiere.model.Query;
import org.compiere.model.X_AD_Reference;
import org.compiere.util.Env;
import org.compiere.util.Language;

public class DisplayTypeFactory implements IDisplayTypeFactory{

	public static int NepaliDatePicker = ((X_AD_Reference) new Query(
			Env.getCtx(), 
			X_AD_Reference.Table_Name, 
			"name='Date'",  // change to date for Nepali Date in all date reference 
			null
	).first()).getAD_Reference_ID();
	
	public static int NepaliInput = ((X_AD_Reference) new Query(
			Env.getCtx(), 
			X_AD_Reference.Table_Name, 
			"name='Nepali Input'",
			null
	).first()).getAD_Reference_ID();
	
	public static int BSDatePicker = ((X_AD_Reference) new Query(
			Env.getCtx(), 
			X_AD_Reference.Table_Name, 
			"name='BS Date'",
			null
	).first()).getAD_Reference_ID();
	
	
	@Override
	public boolean isID(int displayType) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isNumeric(int displayType) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Integer getDefaultPrecision(int displayType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isText(int displayType) {
		if(displayType == NepaliInput ) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isDate(int displayType) {
		if(displayType == NepaliDatePicker || displayType == BSDatePicker) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isLookup(int displayType) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isLOB(int displayType) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public DecimalFormat getNumberFormat(int displayType, Language language, String pattern) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SimpleDateFormat getDateFormat(int displayType, Language language, String pattern) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Class<?> getClass(int displayType, boolean yesNoAsBoolean) {
		if(displayType == NepaliDatePicker || displayType == BSDatePicker) {
			return java.sql.Date.class;
		} else if (displayType == NepaliInput ) {
			return String.class;
		}
		return null;
	}

	@Override
	public String getSQLDataType(int displayType, String columnName, int fieldLength) {
		if(displayType == NepaliDatePicker || displayType == BSDatePicker) {
			return "date";
		} else if (displayType == NepaliInput ) {
			return "text";
		}
		return null;
	}

	@Override
	public String getDescription(int displayType) {
		if(displayType == NepaliDatePicker || displayType == BSDatePicker) {
			return "Nepali Date Picker for Idempiere";
		} else if (displayType == NepaliInput ) {
			return "Nepali Input for Idempiere";
		}
		return null;
	}

}