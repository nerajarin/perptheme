package com.phoenix.uimodules.editor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.editor.WEditor;
import org.adempiere.webui.event.ValueChangeEvent;
import org.apache.commons.codec.binary.StringUtils;
import org.compiere.model.GridField;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.event.EventHandler;
import org.zkoss.html.StyleSheet;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import com.google.common.base.Strings;

import np.com.bahadur.converter.date.nepali.DateConverter;

public class NepaliDatepicker extends WEditor {

	private Textbox component = null;
	private String uniqueId = null;
	private boolean readwrite = true;
	private Object oldValue;

	public NepaliDatepicker(GridField field) {
		super(new Textbox(), field);
		component = (Textbox) this.getComponent();
		uniqueId = component.getUuid() + field.getAD_Column_ID() + field.getVO().SeqNo + field.getAD_Field_ID();
		component.setClass(uniqueId);
		component.addEventListener(Events.ON_CHANGE, this);
		component.addEventListener(Events.ON_FOCUS, this);
		try {
			addDatepickerJavascript();
		} catch (IOException e) {
			throw new AdempiereException(e.getMessage());
		}
	};

	private String getDataFromURL(URL url) throws IOException {
		BufferedReader br;
		br = new BufferedReader(new InputStreamReader(url.openConnection().getInputStream()));
		StringBuilder retString = new StringBuilder();
		while (br.ready()) {
			retString.append(br.readLine() + "\n");
		}
		br.close();
		return retString.toString();
	}

	private void addDatepickerJavascript() throws IOException {

		JSONObject config = new JSONObject();

		config.put("dateFormat", "%y-%m-%d");
		config.put("closeOnDateSelect", true);
		StringBuilder javaScript = new StringBuilder();
		javaScript.append("var el = document.getElementsByClassName('" + uniqueId + "');");
		javaScript.append("if(!$(el[0]).data('nepaliDatePicker')){");
		javaScript.append("$(el[0]).data('nepaliDatePicker', true);");
		javaScript.append("$(el[0]).nepaliDatePicker(");
		javaScript.append(config.toJSONString());
		javaScript.append(")");
		javaScript.append("}");
		Clients.evalJavaScript(javaScript.toString());

		javaScript = new StringBuilder();
		javaScript.append("$(\".");
		javaScript.append(uniqueId);
		javaScript.append("\").on(\"dateChange\", function(event) {");
		javaScript.append("var id = event.target.id; ");
		javaScript.append("var timestamp = event.datePickerData.formattedDate; ");
		javaScript.append("zk.Widget.$(id).setValue(timestamp);");
		javaScript.append("zk.Widget.$(id).fireOnChange();");
		javaScript.append("})");

		Clients.evalJavaScript(javaScript.toString());

	}

	@Override
	public void setReadWrite(boolean readWrite) {
		this.readwrite = readWrite;
	}

	@Override
	public boolean isReadWrite() {
		// TODO Auto-generated method stub
		return this.readwrite;
	}

	@Override
	public void setValue(Object value) {
		oldValue = value;
		try {
			if (value != null) {
				Date d = (Date) value;
				SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
				DateConverter dc = new DateConverter();
				String bsDate = 
						nepaliNumberEng(
								dc.convertAdToBs(format.format(d))
								)
;
				this.component.setValue(bsDate);
			}
			else
			{
				Date d = new Date();
				SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
				DateConverter dc = new DateConverter();
				String bsDate = nepaliNumberEng(dc.convertAdToBs(format.format(d)));
				this.component.setValue(bsDate);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
//		this.component.setValue(value);
	}

	@Override
	public Object getValue() {
//		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		
		String newValue = this.component.getValue();
		Object nValue = null;
		if (newValue != null) {
			String[] parts = newValue.split("-");
			if (parts.length == 3) {
				String day = nepaliNumber(Strings.padStart(parts[2], 2, '0'));
				String month = nepaliNumber(Strings.padStart(parts[1], 2, '0'));
				String year = nepaliNumber(parts[0]);
				DateConverter dc = new DateConverter();
				Date d = dc.convertBsToAd(day + month + year);
				nValue = Timestamp.from(d.toInstant());
			}
		}
		System.out.println("new "+nValue);
//		DateConverter dc = new DateConverter();
		return nValue;
//				dc.convertBsToAd(nepaliNumber(this.component.getValue()));
//		return Timestamp.valueOf(dc.convertBsToAd(nepaliNumber(this.component.getValue())).concat(" 0:0:0.0"));
	}

	@Override
	public String getDisplay() {
		return nepaliNumber(this.component.getValue());
//		return null;
	}

	public String nepaliNumberEng(String str) {
		String retStr = "";
		Map<String, String> m = new HashMap<String, String>();
		m.put("0", "0");
		m.put("1", "1");
		m.put("2", "2");
		m.put("3", "3");
		m.put("4", "4");
		m.put("5", "5");
		m.put("6", "6");
		m.put("7", "7");
		m.put("8", "8");
		m.put("9", "9");
		for (int i = 0; i < str.length(); i++) {
			String c = str.substring(i, i + 1);
			if (m.containsKey(c)) {
				retStr += m.get(c);
			} else {
				retStr += c;
			}
		}
		System.out.println("Return String inside Nepali Number ENg="+retStr);
		return retStr;
	}

	public String nepaliNumber(String str) {
		String retStr = "";
		Map<String, String> m = new HashMap<String, String>();
		m.put("0", "0");
		m.put("1", "1");
		m.put("2", "2");
		m.put("3", "3");
		m.put("4", "4");
		m.put("5", "5");
		m.put("6", "6");
		m.put("7", "7");
		m.put("8", "8");
		m.put("9", "9");
		for (int i = 0; i < str.length(); i++) {
			String c = str.substring(i, i + 1);
			if (m.containsKey(c)) {
				retStr += m.get(c);
			} else {
				retStr += c;
			}
		}
		System.out.println("Return String inside Nepali Number="+retStr);
		return retStr;
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (Events.ON_CHANGE.equalsIgnoreCase(event.getName())) {
			String newValue = this.component.getValue();
			Object nValue = null;
			if (newValue != null) {
				String[] parts = newValue.split("-");
				if (parts.length == 3) {
					String day = nepaliNumber(Strings.padStart(parts[2], 2, '0'));
					String month = nepaliNumber(Strings.padStart(parts[1], 2, '0'));
					String year = nepaliNumber(parts[0]);
					DateConverter dc = new DateConverter();
					Date d = dc.convertBsToAd(day + month + year);
					nValue = Timestamp.from(d.toInstant());
				}
			}
			if (oldValue == null && newValue == null) {
				return;
			}
			ValueChangeEvent changeEvent = new ValueChangeEvent(this, this.getColumnName(), oldValue, nValue);
			super.fireValueChange(changeEvent);
			oldValue = newValue;
		}

		if (Events.ON_FOCUS.equalsIgnoreCase(event.getName())) {
//			System.out.println("on focus called");
			addDatepickerJavascript();
		}
		if (Events.ON_DESKTOP_RECYCLE.equalsIgnoreCase(event.getName())) {
			System.out.println("on desktop recycle called");
		}
		if (Events.ON_VISIBILITY_CHANGE.equalsIgnoreCase(event.getName())) {
			System.out.println("on visibiliy change called");
		}
//		Textbox box = (Textbox) event.getTarget();
//		System.out.println("event: " + box.getValue());
	}

}
