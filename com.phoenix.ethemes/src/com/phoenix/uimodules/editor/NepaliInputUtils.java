package com.phoenix.uimodules.editor;

import java.util.ArrayList;

public class NepaliInputUtils {

	static ArrayList<String> romanizedMapping = new ArrayList<String>() {
		private static final long serialVersionUID = 1L;
		{
			add("\u0020"); // SPACE
			add("\u0021"); // ! -> !
			add("\\u0022"); // " -> "
			add("\u0023"); // # -> #
			add("\u0024"); // $ -> $
			add("\u0025"); // % -> %
			add("\u0026"); // & -> &
			add("\u0027"); // ' -> '
			add("\u0028"); // ( -> (
			add("\u0029"); // ) -> )
			add("\u002A"); // ->
			add("\u200C"); // + -> ZWNJ
			add("\u002C"); // , -> ,
			add("\u002D"); // - -> -
			add("\u0964"); // . -> ।
			add("\u094D"); // / -> ्

			add("\u0966"); // 0 -> ०
			add("\u0967"); // 1 -> १
			add("\u0968"); // 2 -> २
			add("\u0969"); // 3 -> ३
			add("\u096A"); // 4 -> ४
			add("\u096B"); // 5 -> ५
			add("\u096C"); // 6 -> ६
			add("\u096D"); // 7 -> ७
			add("\u096E"); // 8 -> ८
			add("\u096F"); // 9 -> ९
			add("\u003B"); // ; -> ;
			add("\u003A"); // : -> :
			add("\u0919"); // < -> ङ
			add("\u200D"); // = -> ZWJ
			add("\u00B7"); // > -> . // This is not actually a normal fullstop its a Catalan
			add("\u003F"); // ? -> ?
			add("\u0040"); // @ -> @

			add("\u0906"); // A -> आ 65
			add("\u092D"); // B -> भ
			add("\u091A"); // C -> च
			add("\u0927"); // D -> ध
			add("\u0948"); // E -> ै
			add("\u090A"); // F -> ऊ
			add("\u0918"); // G -> घ
			add("\u0905"); // H -> अ
			add("\u0940"); // I -> ी
			add("\u091D"); // J -> झ
			add("\u0916"); // K -> ख
			add("\u0933"); // L -> ळ
			add("\u0902"); // M -> ं
			add("\u0923"); // N -> ण
			add("\u0913"); // O -> ओ
			add("\u092B"); // P -> फ
			add("\u0920"); // Q -> ठ
			add("\u0943"); // R -> ृ
			add("\u0936"); // S -> श
			add("\u0925"); // T -> थ
			add("\u0942"); // U -> ू
			add("\u0901"); // V -> ँ
			add("\u0914"); // W -> औ
			add("\u0922"); // X -> ढ
			add("\u091E"); // Y -> ञ
			add("\u090B"); // Z -> ऋ 90
			add("\u0907"); // [ -> इ
			add("\u0950"); // \ -> ॐ
			add("\u090F"); // ] -> ए
			add("\u005E"); // ^ -> ^
			add("\u0952"); // _ ->"॒"
			add("\u093D"); // ` -> ऽ

			add("\u093E"); // a -> ा 97
			add("\u092C"); // b -> ब
			add("\u091B"); // c -> छ
			add("\u0926"); // d -> द
			add("\u0947"); // e -> े
			add("\u0909"); // f -> उ
			add("\u0917"); // g -> ग
			add("\u0939"); // h -> ह
			add("\u093F"); // i -> ि
			add("\u091C"); // j -> ज
			add("\u0915"); // k -> क
			add("\u0932"); // l -> ल
			add("\u092E"); // m -> म
			add("\u0928"); // n -> न
			add("\u094B"); // o -> ो
			add("\u092A"); // p -> प
			add("\u091F"); // q -> ट
			add("\u0930"); // r -> र
			add("\u0938"); // s -> स
			add("\u0924"); // t -> त
			add("\u0941"); // u -> ु
			add("\u0935"); // v -> व
			add("\u094C"); // w -> ौ
			add("\u0921"); // x -> ड
			add("\u092F"); // y -> य
			add("\u0937"); // z -> ष 122
			add("\u0908"); // { -> ई
			add("\u0903"); // | -> ः
			add("\u0910"); // } -> ऐ
			add("\u093C"); // ~ -> "़"

		}
	};

	static String convertToNepali(String str) {
		StringBuilder sb = new StringBuilder();
		char[] letters = str.toCharArray();

		for (char ch : letters) {
			int charCode = (int) ch - 32;
			if (romanizedMapping.size() > charCode) {
				sb.append(romanizedMapping.get(charCode));
			} else {
				sb.append(ch);
			}
		}
		return sb.toString();
	}

}
