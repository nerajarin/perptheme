package com.phoenix.uimodules.editor;

import java.io.IOException;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.editor.WEditor;
import org.adempiere.webui.event.ValueChangeEvent;
import org.compiere.model.GridField;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;

public class NepaliInput extends WEditor {

    private Textbox component = null;
	private String uniqueId = null;
	private String oldValue;

	public NepaliInput(GridField field) {
		super(new Textbox(), field);
		component = (Textbox) this.getComponent();
		uniqueId = component.getUuid() + field.getAD_Column_ID() + field.getVO().SeqNo + field.getAD_Field_ID();
		component.setSclass(uniqueId);

		component.addEventListener(Events.ON_CHANGE, this);
		component.addEventListener(Events.ON_FOCUS, this);
		
		try {
			setTranslationListener();
		} catch (IOException e) {
			throw new AdempiereException(e.getMessage());
		}
		
	}

	private void setTranslationListener() throws IOException {
		Clients.evalJavaScript("setListener('"+uniqueId+"')");
	}

	@Override
	public void setReadWrite(boolean readWrite) {
	}

	@Override
	public boolean isReadWrite() {
		return false;
	}

	@Override
	public void setValue(Object value) {
		if (value != null) {			
			this.component.setValue(value.toString());
		} else {
			this.component.setValue("");
		}
		oldValue = this.component.getValue();
	}

	@Override
	public Object getValue() {
		return oldValue;
	}

	@Override
	public String getDisplay() {
		return this.component.getValue();
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (Events.ON_CHANGE.equalsIgnoreCase(event.getName())) {

			String newValue = this.component.getValue();
			if (oldValue == null && newValue == null) {
				return;
			}
			ValueChangeEvent changeEvent = new ValueChangeEvent(this, this.getColumnName(), oldValue, newValue);
			super.fireValueChange(changeEvent);
			oldValue = this.component.getValue();

		}
		if(Events.ON_FOCUS.equalsIgnoreCase(event.getName())) {
			setTranslationListener();
		}

	}

}
