package com.phoenix.ethemes.panel;

import java.io.IOException;

import org.adempiere.webui.ClientInfo;
import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.panel.UserPanel;
import org.adempiere.webui.theme.ThemeManager;
import org.compiere.model.MImage;
import org.compiere.model.MOrg;
import org.compiere.model.MRole;
import org.compiere.model.MUser;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.zkoss.image.AImage;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zul.Image;

public class PUserPanel extends UserPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6442758984675935215L;
	static CLogger log = CLogger.getCLogger(PHeaderPanel.class);
	public static String THEME_USER_IMAGE_PATH = "images/Avatar.png";

	@Override
	protected void onCreate() {
		super.onCreate();
		Image image = (Image) component.getFellowIfAny("user_image");
		if (image != null) {
			try {
				org.zkoss.image.Image userImage = this.getUserImage();
				if (userImage != null) {
					image.setContent(userImage);
				} else {
					image.setSrc(ThemeManager.getThemeResource(PUserPanel.THEME_USER_IMAGE_PATH));
				}
				image.setVisible(true);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		lblUserNameValue.setValue(getOrgName() + "\n" + getUserName() );
		if (ClientInfo.isMobile())
    	{	
			lblUserNameValue.setMultiline(false);
    		lblUserNameValue.setValue(getUserName());
    		LayoutUtils.addSclass("mobile", (HtmlBasedComponent) component);
    	}
	}

	private org.zkoss.image.Image getUserImage() throws IOException {
		MUser user = MUser.get(ctx);
		if (user.getAD_Image_ID() > 0) {
			MImage mImage = MImage.get(ctx, user.getAD_Image_ID());
			if (mImage.getData() != null)
				return new AImage(mImage.getName(), mImage.getData());
			else
				return null;

			/*
			 * Using different approach: ImageEncoder supports only PNG and JPEG Image image
			 * = mImage.getImage(); if (image instanceof RenderedImage) { RenderedImage
			 * rImage = (RenderedImage)image; return Images.encode(mImage.getName(),
			 * rImage); } else { BufferedImage bImage = new
			 * BufferedImage(image.getWidth(null), image.getHeight(null),
			 * BufferedImage.TYPE_INT_ARGB); Graphics2D bImageGraphics =
			 * bImage.createGraphics(); bImageGraphics.drawImage(image, null, null);
			 * RenderedImage rImage = (RenderedImage)bImage; String name = mImage.getName();
			 * if (name.endsWith("jpg")) { name = name.replace("jpg", "jpeg"); } return
			 * Images.encode(name, rImage); }
			 */
		} else {
			return null;
		}
	}

	private String getUserName() {
		MUser user = MUser.get(ctx);
		return user.getName();
	}

	private String getRoleName() {
		MRole role = MRole.getDefault(ctx, false);
		return role.getName();
	}

	private String getOrgName() {
		int orgId = Env.getAD_Org_ID(ctx);
		if (orgId > 0) {
			MOrg org = MOrg.get(ctx, orgId);
			return org.getName();
		} else {
			return "-";
		}
	}

}
