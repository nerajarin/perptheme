package com.phoenix.ethemes.webui.alert;

import java.util.ArrayList;
import java.util.List;

import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.compiere.model.MDocumentStatus;
import org.compiere.util.CLogger;
import org.compiere.util.Env;

public class WDocumentStatusPanel extends Panel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7473476079783059880L;

	private List<WDocumentStatusIndicator> indicatorList = new ArrayList<WDocumentStatusIndicator>();

	/** Document Status Indicators	*/
	private MDocumentStatus[] 	m_indicators = null;

	/**	Logger	*/
	private static final CLogger log = CLogger.getCLogger (WDocumentStatusPanel.class);

	/**
	 * 	Get Panel if User has Document Status Indicators
	 *	@return panel
	 */
	public static WDocumentStatusPanel get()
	{
		int AD_User_ID = Env.getAD_User_ID(Env.getCtx());
		int AD_Role_ID = Env.getAD_Role_ID(Env.getCtx());
		MDocumentStatus[] indicators = MDocumentStatus.getDocumentStatusIndicators(Env.getCtx(), AD_User_ID, AD_Role_ID);
		return new WDocumentStatusPanel(indicators);
	}

	/**************************************************************************
	 * 	Constructor
	 *	@param Document Status Indicators
	 */
	private WDocumentStatusPanel (MDocumentStatus[] indicators)
	{
		super ();
		m_indicators = indicators;
		init();
	}

	/**
	 * 	Static/Dynamic Init
	 */
	private void init()
	{
		log.info("");
		Grid grid = new Grid();
		appendChild(grid);
		grid.setWidth("100%");
		grid.makeNoStrip();
		grid.setOddRowSclass("even");

		Rows rows = new Rows();
		grid.appendChild(rows);

		for (int i = 0; i < m_indicators.length; i++)
		{
			Row row = new Row();
			rows.appendChild(row);

			WDocumentStatusIndicator pi = new WDocumentStatusIndicator(m_indicators[i]);
			row.appendChild(pi);
			indicatorList.add(pi);
		}
	}	//	init

	public void refresh() {
		for (WDocumentStatusIndicator indicator : indicatorList) {
			indicator.refresh();
		}
	}

	public void updateUI() {
		for (WDocumentStatusIndicator indicator : indicatorList) {
			indicator.updateUI();
		}
	}
}
