<%@ page contentType="text/css;charset=UTF-8" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.idempiere.org/dsp/web/util" prefix="u" %>

html,body {
	margin: 0;
	padding: 0;
	height: 100%;
	width: 100%;
	background-color: #D4E3F4;
	color: #333;
	font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;
	overflow: hidden;
}

.z-html p{
	margin:0px;
}

.z-north-body, .z-south-body, .z-west-body, .z-center-body, .z-east-body{
	padding:0px;
}

.z-panelchildren{
	padding:0px;
}

input[type="checkbox"]{
	margin:0px;
	width: 18px;
	height: 18px;
}

.z-tabs-content{
	background: #E4E4E4;
}

.z-east-icon, .z-west-icon, .z-north-icon, .z-south-icon{
	//font-size:32px;
}

.z-toolbar.z-toolbar-tabs{
	background: #E4E4E4;
}
.z-toolbarbutton:hover{
	background: #C4C4C4;
}

.z-row:hover > .z-row-inner, .z-row:hover > .z-cell{
	//background: #C4C4C4;
}

.z-north-slide > .z-north-collapsed:hover, .z-north-slide > .z-south-collapsed:hover, .z-north-slide > .z-west-collapsed:hover, .z-north-slide > .z-center-collapsed:hover, .z-north-slide > .z-east-collapsed:hover, .z-south-slide > .z-north-collapsed:hover, .z-south-slide > .z-south-collapsed:hover, .z-south-slide > .z-west-collapsed:hover, .z-south-slide > .z-center-collapsed:hover, .z-south-slide > .z-east-collapsed:hover, .z-west-slide > .z-north-collapsed:hover, .z-west-slide > .z-south-collapsed:hover, .z-west-slide > .z-west-collapsed:hover, .z-west-slide > .z-center-collapsed:hover, .z-west-slide > .z-east-collapsed:hover, .z-center-slide > .z-north-collapsed:hover, .z-center-slide > .z-south-collapsed:hover, .z-center-slide > .z-west-collapsed:hover, .z-center-slide > .z-center-collapsed:hover, .z-center-slide > .z-east-collapsed:hover, .z-east-slide > .z-north-collapsed:hover, .z-east-slide > .z-south-collapsed:hover, .z-east-slide > .z-west-collapsed:hover, .z-east-slide > .z-center-collapsed:hover, .z-east-slide > .z-east-collapsed:hover {
    background: #C4C4C4;
}
window-container-toolbar-btn.z-toolbarbutton .z-toolbarbutton-content [class^="z-icon"]{
	font-size:32px;
}

.z-label, .z-radio-content, .z-checkbox-content, .z-loading {
//	font-family: arial; for z-icon not displayed
}

[class*="z-"]:not([class*="z-icon-"]):not([class*="z-group-icon-"]) {
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;
}
@media screen and (min-device-width: 2500px) {
	[class*="z-"]:not([class*="z-icon-"]):not([class*="z-group-icon-"]) {
		font-size: 15px;
	}
}
@media screen and (max-device-width: 2499px) {
	[class*="z-"]:not([class*="z-icon-"]):not([class*="z-group-icon-"]) {
		font-size: 14px;
	}
}
@media screen and (max-device-width: 1899px) {
	[class*="z-"]:not([class*="z-icon-"]):not([class*="z-group-icon-"]) {
		font-size: 13px;
	}
}
@media screen and (max-device-width: 1399px) {
	[class*="z-"]:not([class*="z-icon-"]):not([class*="z-group-icon-"]) {
		font-size: 12px;
	}
}

<%-- Mobile/Tablet --%>
.tablet-scrolling {
	-webkit-overflow-scrolling: touch;
}
<%-- default font size for mobile --%>
.mobile [class*="z-"]:not([class*="z-icon-"]):not([class*="z-group-icon-"]) {
    font-size: 16px;
}
<%-- the not=xyz is needed to get this selected over standard zk rule --%>
.mobile [class*="z-icon-"]:not([class*="xyz"]), .mobile [class*="z-group-icon-"] {
    font-size: 16px;
}

<%-- vbox fix for firefox and ie --%>
table.z-vbox > tbody > tr > td > table {
	width: 100%;	
}

<%-- workflow activity --%>
.workflow-activity-form {
}
.workflow-panel-table {
	border: 0px;
}

<%-- payment form --%>
.payment-form-content {
}

<c:include page="fragment/login.css.dsp" />

<c:include page="fragment/desktop.css.dsp" />

<c:include page="fragment/application-menu.css.dsp" />

<c:include page="fragment/gadget.css.dsp" />

<c:include page="fragment/toolbar.css.dsp" />

/** <c:include page="fragment/button.css.dsp" /> **/

<c:include page="fragment/adwindow.css.dsp" />
			
<c:include page="fragment/grid.css.dsp" />

<c:include page="fragment/input-element.css.dsp" />

/** Menu Tree <c:include page="fragment/tree.css.dsp" /> **/ 

<c:include page="fragment/field-editor.css.dsp" />

<c:include page="fragment/group.css.dsp" />

<c:include page="fragment/tab.css.dsp" />

<c:include page="fragment/menu-tree.css.dsp" />

<c:include page="fragment/info-window.css.dsp" />

<c:include page="fragment/window.css.dsp" /> /** Modal and window color **/

<c:include page="fragment/form.css.dsp" />

<c:include page="fragment/toolbar-popup.css.dsp" />	

<c:include page="fragment/setup-wizard.css.dsp" />

<c:include page="fragment/about.css.dsp" />

<c:include page="fragment/tab-editor.css.dsp" />

<c:include page="fragment/find-window.css.dsp" />

<c:include page="fragment/help-window.css.dsp" />

<c:include page="fragment/borderlayout.css.dsp" />

<c:include page="fragment/parameter-process.css.dsp" />

<c:include page="fragment/window-size.css.dsp" />

<c:include page="fragment/font-icons.css.dsp" />

<c:if test="${u:isThemeHasCustomCSSFragment()}">
    <c:include page="fragment/custom.css.dsp" />
</c:if>